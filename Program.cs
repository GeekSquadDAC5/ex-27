﻿using System;
using System.Collections.Generic;

namespace ex_27
{

    // 1. Create a class called Items
    // Set Fields for Price and Quantity

    // 2. Create a class called Checkout
    // Create a List that takes the class Items as the type
    // Create a method that calculate the tax
    // Create a method that makes the purchase

    // 3. In your main method create the following:
    // Declare the required variables
    // Do / while loop asking for a quantity and a price then ask if they want to make another purchase

    // 4. When they are done buying stuff calculate the total price
    // 5. List all the quantities and prices that are recorded in the List
    // 6. Show the total of the purchase including GST and Farewell the customer

    class Items
    {
        // For product price;
        public double Price;

        // For product quantity
        public int Quantity;

        public Items()
        {
            // Init
            this.Price = 0;
            this.Quantity = 0;
        }
    }

    class Checkout
    {
        // Contains instances of class 'Items'
        private List<Items> ItemList;

        public Checkout()
        {
            // Init : Creats instance of List collection class with instances of class 'Items' as values;
            ItemList = new List<Items>();
        }

        /*
         * Makes a purchase with price and quantities,
         * and assigns the values into appropriated fields of class Items.
         */
        public void Purchase(double price, int quantity)
        {
            var items = new Items();
            
            items.Price = price;
            items.Quantity = quantity;

            this.ItemList.Add(items);
        }

        /*
         * Calculates the GST
         * Default rate of GST : 15%
         */
        public double GetGST(double price, double rate = 15)
        {
            return price * rate/100;
        }

        /*
         * Calculates sub-total price, and return the value
         */
        public double GetSubTotal()
        {
            double total = 0.00;

            foreach(var item in this.ItemList)
            {
                total = total + item.Price * item.Quantity;
            }

            return total;
        }

        /*
         * Calculates grand total price(inc. GST), and return the value
         */
        public double GetGrandTotal()
        {
            double subTotal = this.GetSubTotal();
            double GST = this.GetGST(subTotal);

            return subTotal + GST;
        }

        public double GetGrandTotal(double subTotal, double GST)
        {
            return subTotal + GST;
        }

        /*
         * Prints out a receipt
         */
        public void PrintOutReceipt()
        {
            // Print out a message
            Console.WriteLine("------------------------------------");
            Console.WriteLine("        Your purchase list");
            Console.WriteLine("------------------------------------");

            var i = 0;
            foreach(var item in this.ItemList)
            {
                 Console.WriteLine($" Product{i}    : {item.Price:C2} x {item.Quantity} = {item.Price*item.Quantity:C2}");
            }

            var subTotal = this.GetSubTotal();
            var GST = this.GetGST(subTotal);
            var gradnTotal = this.GetGrandTotal();

            Console.WriteLine("------------------------------------");
            Console.WriteLine($" Sub Total   : {this.GetSubTotal():C2}");
            Console.WriteLine($" GST         : {this.GetGST(subTotal):C2}");
            Console.WriteLine($" Grand Total : {this.GetGrandTotal(subTotal, GST):C2}");
            Console.WriteLine($"------------------------------------");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            // Creates new instance of class Checkout.
            var checkout = new Checkout();

            // Declares variables
            double price;  
            int quantity;
            string answer;
            bool isAdded = false;

            // Print out a welcome message
            Console.WriteLine("------------------------------------");
            Console.WriteLine("       Welcome to Sing's Shop");
            Console.WriteLine("------------------------------------\n");

            // Makes a purchase
            do
            {
                Console.Write("> How much is the product? $");
                price = double.Parse(Console.ReadLine());

                Console.Write("> How many products do you want to buy? ");
                quantity = int.Parse(Console.ReadLine());

                // Saves the price and quantity in fields of instance checkout.
                checkout.Purchase(price, quantity);

                Console.Write("> Do you want to add another product? ('yes' or 'no') : ");
                answer = Console.ReadLine();
                isAdded = (answer.ToLower() == "yes" || answer.ToLower() =="y") ? true : false;

                Console.WriteLine("\n");

            } while(isAdded);

            // Prints out a receipt with sub-total, GST and grand-total
            checkout.PrintOutReceipt();
            Console.WriteLine("\n");

            // Print out a goodbye message
            Console.WriteLine("------------------------------------");
            Console.WriteLine("             Thank you!");
            Console.WriteLine("------------------------------------\n");
        }
    }
}
